import json
import numpy as np
import os
import matplotlib.pyplot as plt
from os.path import exists
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--filename')

args = parser.parse_args()

js = json.load(open(args.filename))

data = np.array(js['accuracies'],dtype=float)

print(np.mean(data))



