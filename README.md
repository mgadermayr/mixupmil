# MixUpMIL

This code refers to the paper
"MixUp-MIL: Novel Data Augmentation for Multiple Instance Learning and a Study on Thyroid Cancer Diagnosis"
presented at MICCAI'23.

A preprint is available via arxiv:
https://arxiv.org/pdf/2211.05862.pdf

A teaser video is available here:
https://www.youtube.com/watch?v=Zuobi6sdlAg&ab_channel=MichaelGadermayr

Citation: 

@inproceedings{gadermayr2022mixup,\
  title={MixUp-MIL: Novel Data Augmentation for Multiple Instance Learning and a Study on Thyroid Cancer Diagnosis},\
  author={Gadermayr, Michael and Koller, Lukas and Tschuchnig, Maximilian and Stangassinger, Lea Maria and Kreutzer, Christina and Couillard-Despres, Sebastien and Oostingh, Gertie Janneke and Hittmair, Anton},\
  journal={Proceedings of the 26th International Conference on Medical Image Computing and Computer Assisted Intervention (MICCAI)},\
  year={2023}\
}

## Getting started

Setup a conda environment 
> conda env create --name Pytorch --file=conda.yml

Activate the environment
> conda activate Pytorch

Run the code (including random spliting
> python dsmil-wsi/train_test.py --dataset=512_Froz/ --num_classes=2 --split=0.2 --feats_size=512 --num_epochs 200 --num_randsplits 32 --instance_weight 0.5 --result_string 'TEST'

## License
The source code is published under the MIT License 

## Credits
The code is based on the implementation of the dual-stream multiple instance learning framework provided under:

https://github.com/binli123/dsmil-wsi

Citation:

@inproceedings{li2021dual,\
  title={Dual-stream multiple instance learning network for whole slide image classification with self-supervised contrastive learning},\
  author={Li, Bin and Li, Yin and Eliceiri, Kevin W},\
  booktitle={Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition},\
  pages={14318--14328},\
  year={2021}\
}
